**Описание Проекта**

Пиложения для маининга, докерезирован и развернут на docker-swarm. Подключен к монитоингу Prometheus, Grafana. Получем логи на Loki.


**Инфраструктура**

  1. Инфраструктура разварачивается на Vagrant, провайдером выступает VirtualBox
  2. Vagrantfile лежит в папке vagrant, переменные вынесены в отдельный файл "settings.yaml"
  3. В переменные можем изменить колличетсво swarm-manager, swarm-worker и cpu, memory

**Все остальное раскатывет Ansible**

  1. Устанавливает docker
  2. Инициализирует docker-swarm и подключает к нему остальных менеджер и воркеров
  3. Деплоит проект модулем docker_stack
  4. Устанавливает node-exporeter и promtail на хостах докера
  5. Устанавливает на отдльную машину Prometheus, Grafana, Loki и настривает их

**Что следует даработать:**

  1. Вывести на вагрант provisioner ansible
  2. Все изменяемые переменные в ролях вывести отдельно
  3. Поставить нормальные дашборды для графаны 
  4. Даработать docker-compose.yml приложения. Сделать так что бы колличество реплик была задано по колличеству swarm-worker. 
  5. В вагрант назначить дефолтный интерфейс enp0s3 вместо enp0s8. И заменить во всех ролях.
